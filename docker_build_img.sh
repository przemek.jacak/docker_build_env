#!/usr/bin/env bash
set -e

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	versions=$( ls images/ |grep Dockerfile_ | cut -f2 -d'_' )
fi

for v in $versions; do 
	docker build -t pjacak/build_env:$v -f images/Dockerfile_$v images/
done
