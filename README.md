#My Docker build env

To rebuild images call
`./docker_build_img.sh` 

To publish images call
`./docker_push_img.sh`

To compile project using those images call:
`docker run --rm -v "$PWD"/ccache:/ccache -v "$PWD":/usr/src/myapp -w /usr/src/myapp/build pjacak/build_env:<BUILD TAG> /bin/bash -c "cmake .. ; make ; make test"`
